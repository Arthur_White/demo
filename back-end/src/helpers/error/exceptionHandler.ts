import { Response } from 'express';

const exceptionHandler = (err: any, res: Response) => {
  res.status(err.status || 500);
  return res.json({
    message: err.message,
    error: err.stack,
  });
};

export default exceptionHandler;
