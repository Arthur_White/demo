import { Response } from 'express';

const noSuchDataErrorHandler = (res: Response, message: string) => {
  return res.status(404).json({ message });
};

export default noSuchDataErrorHandler;
