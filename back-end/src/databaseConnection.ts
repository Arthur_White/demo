import { Connection, createConnection } from 'typeorm';

const entities = [
  `${__dirname}/database/entities/*.ts`,
  `${__dirname}/database/entities/*.js`,
];

const createDatabaseConnection = async (): Promise<Connection> => {
  return createConnection({
    type: process.env.DATABASE_TYPE as any,
    host: process.env.DATABASE_HOST,
    port: +(process.env.DATABASE_PORT as any),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities,
    synchronize: true,
  });
};

export default createDatabaseConnection;
