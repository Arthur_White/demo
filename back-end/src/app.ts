import 'dotenv/config';
import cors from 'cors';
import express from 'express';

import routes from './routes';
import connectionWrapper from './connectionWrapper';

const Logger = (message: string) => console.log(message);
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(routes);

connectionWrapper(() => {
  const port = process.env.PORT || 3000;
  const { pid } = process;

  const server = app.listen(port, () => {
    Logger(`🚀 Server listening at port ${port} with PID ${pid}`);
  });

  const closeOpenConnections = (errorOccurred: boolean) => {
    Logger('Shutting down server and open connections');
    server.close(() => {
      Logger('Server shut down');
      // Close db connection and other connections
      process.exit(errorOccurred ? 1 : 0);
    });
  };

  process.on('unhandledRejection', (reason: any): void => {
    throw reason;
  });

  process.on('uncaughtException', (error: Error) => {
    Logger(error);
    closeOpenConnections(true);
  });

  process.on('SIGTERM', () => {
    Logger('SIGTERM Signal Received');
    closeOpenConnections(false);
  });
});
