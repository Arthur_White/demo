import userDAO from '../database/dao/userDAO';

export default class UserService {
  public static userFromDbIfExist = async (deviceID: string) => {
    const users = await userDAO.getUserByDeviceID(deviceID);
    return users.length === 0 ? null : users[0];
  };

  public static updateOrCreateUserIfNotExist = async (
    deviceID: string,
    fcmToken: string
  ) => {
    const user = await UserService.userFromDbIfExist(deviceID);
    if (!user || user.fcmToken !== fcmToken) {
      return userDAO.createUser(deviceID, fcmToken);
    }
    return user;
  };
}
