import {
  messaging,
  initializeApp,
  credential,
  ServiceAccount,
} from 'firebase-admin';
import serviceAccount from '../../firebase-private-key.json';

interface DataMessagePayload {
  [key: string]: string;
}

class FirebaseService {
  constructor() {
    initializeApp({
      credential: credential.cert(serviceAccount as ServiceAccount),
      databaseURL: process.env.FIREBASE_DATABASE_URL,
    });
  }

  /**
   * @param registrationTokens - is array of unique tokens which gives to every devices (Android, IOS)
   * @param notificationObject - object of notification
   * @param dataObject - object of additional data
   */
  public sendMessage = (
    registrationTokens: Array<string>,
    notificationObject: { title: string; body: string },
    dataObject?: DataMessagePayload
  ) => {
    const payload = {
      notification: notificationObject,
      data: dataObject,
    };
    return messaging().sendToDevice(registrationTokens, payload);
  };
}

export default new FirebaseService();
