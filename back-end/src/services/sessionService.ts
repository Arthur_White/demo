import sessionDAO from '../database/dao/sessionDAO';

export default class SessionService {
  public static createSession = async (sessionID: string, token: string) => {
    return sessionDAO.createSession(sessionID, token);
  };
}
