import ticketDAO from '../database/dao/ticketDAO';
import Session from '../database/entities/Session';
import User from '../database/entities/User';

export default class TicketService {
  public static ticketFromDbIfExist = async (crmID: string) => {
    const tickets = await ticketDAO.getTicketByCrmID(crmID);
    return tickets.length === 0 ? null : tickets[0];
  };

  public static createTicket = async (
    ticketID: string,
    session: Session,
    user?: User
  ) => {
    return ticketDAO.createTicket(ticketID, session, user);
  };
}
