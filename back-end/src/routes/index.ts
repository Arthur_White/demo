import express, { Request, Response } from 'express';

import userRouter from './userRouter';
import ticketRouter from './ticketRouter';
import sessionRouter from './sessionRouter';

const router = express.Router();

router.use('/user', userRouter);
router.use('/tickets', ticketRouter);
router.use('/session', sessionRouter);

router.all('*', (req: Request, res: Response) => {
  return res.status(404).json({
    status: 'fail',
    message: `Can't find ${req.originalUrl} on this server!`,
  });
});

export default router;
