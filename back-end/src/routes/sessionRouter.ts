import express from 'express';

import sessionController from '../controllers/sessionController';

const router = express.Router();

router.post('/operator-entrance/:ticketID', sessionController.operatorEntrance);

export default router;
