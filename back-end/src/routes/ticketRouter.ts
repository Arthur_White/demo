import express from 'express';

import ticketController from '../controllers/ticketController';

const router = express.Router();

router.get('/ticket-session/:ticketID', ticketController.getTicketSession);
router.post(
  '/create-ticket-with-session',
  ticketController.createTicketWithSession
);

export default router;
