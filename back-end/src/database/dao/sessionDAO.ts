import { getRepository } from 'typeorm';
import Session from '../entities/Session';

export default class SessionDAO {
  public static createSession = async (sessionID: string, token: string) => {
    const session = new Session();
    session.sessionID = sessionID;
    session.token = token;
    return getRepository(Session).save(session);
  };
}
