import { getRepository, createQueryBuilder } from 'typeorm';
import User from '../entities/User';

export default class UserDAO {
  public static createUser = async (deviceID: string, fcmToken: string) => {
    const user = new User();
    user.deviceID = deviceID;
    user.fcmToken = fcmToken;
    await createQueryBuilder()
      .insert()
      .into(User)
      .values(user)
      .onConflict(`("deviceID") DO UPDATE SET "fcmToken" = :fcmToken`)
      .setParameter('fcmToken', user.fcmToken)
      .execute();

    const createdUsers = await UserDAO.getUserByDeviceID(deviceID);
    return createdUsers[0];
  };

  public static getUserByDeviceID = async (deviceID: string) => {
    return getRepository(User).find({
      where: {
        deviceID,
      },
    });
  };
}
