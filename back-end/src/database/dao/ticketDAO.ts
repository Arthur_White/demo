import { getRepository } from 'typeorm';
import Ticket from '../entities/Ticket';
import Session from '../entities/Session';
import User from '../entities/User';

export default class TicketDAO {
  public static getAllTickets = async () => {
    return getRepository(Ticket).find();
  };

  public static getTicketByCrmID = async (crmID: string) => {
    return getRepository(Ticket).find({
      where: {
        crmID,
      },
      relations: ['session', 'user'],
    });
  };

  public static createTicket = async (
    ticketID: string,
    session: Session,
    user?: User
  ) => {
    const ticket = new Ticket();
    ticket.crmID = ticketID;
    ticket.session = session;
    if (user) ticket.user = user;

    return getRepository(Ticket).save(ticket);
  };
}
