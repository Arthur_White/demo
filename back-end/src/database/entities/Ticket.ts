/* eslint-disable import/no-cycle */
import { Entity, Column, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import GenericEntity from './GenericEntity';
import Session from './Session';
import User from './User';

@Entity()
export default class Ticket extends GenericEntity {
  @Column()
  crmID: string;

  @ManyToOne(
    () => User,
    user => user.tickets
  )
  user?: User;

  @OneToOne(() => Session)
  @JoinColumn()
  session: Session;
}
