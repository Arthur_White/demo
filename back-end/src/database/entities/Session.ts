import { Entity, Column } from 'typeorm';
import GenericEntity from './GenericEntity';

@Entity()
export default class Session extends GenericEntity {
  @Column()
  token: string;

  @Column()
  sessionID: string;
}
