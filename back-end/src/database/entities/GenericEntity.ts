import { PrimaryGeneratedColumn } from 'typeorm';

export default abstract class GenericEntity {
  @PrimaryGeneratedColumn('uuid')
  id: number;
}
