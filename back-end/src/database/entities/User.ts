/* eslint-disable import/no-cycle */
import { Entity, Column, OneToMany } from 'typeorm';
import GenericEntity from './GenericEntity';
import Ticket from './Ticket';

@Entity()
export default class User extends GenericEntity {
  @Column({ unique: true })
  deviceID: string;

  @Column()
  fcmToken: string;

  @OneToMany(
    () => Ticket,
    ticket => ticket.user
  )
  tickets: Array<Ticket>;
}
