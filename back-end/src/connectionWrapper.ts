import createDatabaseConnection from './databaseConnection';
const Logger = (message: string) => console.log(message);

const connectionWrapper = async (body: () => void) => {
  const variableNames = [
    'DATABASE_TYPE',
    'DATABASE_HOST',
    'DATABASE_PORT',
    'DATABASE_USERNAME',
    'DATABASE_PASSWORD',
    'DATABASE_NAME',
  ];
  const missingVariables = variableNames.filter((value) => !process.env[value]);
  if (missingVariables.length === 0) {
    await createDatabaseConnection();
    body();
  } else {
    const missingVariablesString = missingVariables.join(', ');
    Logger(
      `The following environmental variables are missing: ${missingVariablesString}.`
    );
  }
};

export default connectionWrapper;
