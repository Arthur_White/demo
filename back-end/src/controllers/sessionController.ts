import { Request, Response } from 'express';

import userService from '../services/userService';
import ticketService from '../services/ticketService';
import firebaseService from '../services/firebaseService';
import exceptionHandler from '../helpers/error/exceptionHandler';
import noSuchDataErrorHandler from '../helpers/error/noSuchDataErrorHandler';

const operatorEntranceMessage = {
  title: 'Operator available',
  body: 'An operator has been connected to your session',
};

export default class SessionController {
  public static operatorEntrance = async (req: Request, res: Response) => {
    try {
      const { ticketID } = req.params;

      const ticket = await ticketService.ticketFromDbIfExist(ticketID);
      if (!ticket) return noSuchDataErrorHandler(res, 'No such ticket');

      if (!ticket.user)
        return noSuchDataErrorHandler(res, `The ticket doesn't contain a user`);

      const user = await userService.userFromDbIfExist(ticket.user.deviceID);
      if (!user) return noSuchDataErrorHandler(res, 'No such user');

      await firebaseService.sendMessage(
        [user.fcmToken],
        operatorEntranceMessage,
        {
          ticketID,
          type: 'OPERATOR_JOINED',
        }
      );
      return res.status(200).send('Ok');
    } catch (err) {
      return exceptionHandler(err, res);
    }
  };
}
