import { Request, Response } from 'express';

import userService from '../services/userService';
import noSuchDataErrorHandler from '../helpers/error/noSuchDataErrorHandler';

export default class UserController {
  public static setFcmToken = async (req: Request, res: Response) => {
    const deviceID = req.headers['device-id'] as string;
    const { fcmToken } = req.body;
    if (!deviceID || !fcmToken)
      return noSuchDataErrorHandler(
        res,
        'No required fields deviceID or fcmToken'
      );

    const user = await userService.updateOrCreateUserIfNotExist(
      deviceID,
      fcmToken
    );
    return res.status(200).json(user);
  };
}
