import { Request, Response } from 'express';

import userService from '../services/userService';
import ticketService from '../services/ticketService';
import sessionService from '../services/sessionService';
import exceptionHandler from '../helpers/error/exceptionHandler';
import noSuchDataErrorHandler from '../helpers/error/noSuchDataErrorHandler';

export default class TicketController {
  private static createSessionAndTicket = async (deviceID?: string) => {
    // Some code to get ticket from additional service
    const ticket = { id: '123' };

    // Some code to get session from additional service
    const sessionID = '1';
    const token = '2';

    let user;
    if (deviceID) {
      const buffUser = await userService.userFromDbIfExist(deviceID);
      if (!buffUser) {
        throw new Error('No such user in db');
      }
      user = buffUser;
    }
    const session = await sessionService.createSession(sessionID, token);
    await ticketService.createTicket(ticket.id, session, user);

    return { sessionID, token };
  };

  public static createTicketWithSession = async (
    req: Request,
    res: Response
  ) => {
    const deviceID = req.headers['device-id'] as string;
    if (!deviceID)
      return noSuchDataErrorHandler(res, 'deviceID variable does not exist');

    try {
      const {
        sessionID,
        token,
      } = await TicketController.createSessionAndTicket(deviceID);

      return res.status(200).json({ sessionID, token });
    } catch (err) {
      return exceptionHandler(err, res);
    }
  };

  public static getTicketSession = async (req: Request, res: Response) => {
    const { ticketID } = req.params;
    const ticket = await ticketService.ticketFromDbIfExist(ticketID);
    if (ticket) {
      const { token, sessionID } = ticket.session;
      return res.status(200).json({ token, sessionID });
    }
    return noSuchDataErrorHandler(res, 'Ticket does not exist!');
  };
}
