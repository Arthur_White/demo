# Back-end code

Here is a code that can show the architectural and writing code style. Here you can see writing code in OOP style and procedural (using files as modules). This code is written using typescript and all the necessary modules for a correct uniform code style (linter, prettier, husky).

This code is not a complete application, since most of it is under the NDA, which does not allow giving access to it. This code does not carry any intellectual property.