const SET_SPEED = 'speed/SET_SPEED';

const initialState = {
    speed: 1
};

const speedReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_SPEED:
            return { ...state, speed: action.speed };
        default:
            return state;
    }
};

export const setSpeed = (speed) => ({ type: SET_SPEED, speed });

export default speedReducer;