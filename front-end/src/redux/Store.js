import thunkMiddleware from 'redux-thunk';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import CountdownReducer from './CountdownReducer';
import SpeedReducer from './SpeedReducer';

const reducers = combineReducers({
    countdown: CountdownReducer,
    speed: SpeedReducer
});

const store = createStore(reducers, applyMiddleware(thunkMiddleware));
window.store = store;

export default store;