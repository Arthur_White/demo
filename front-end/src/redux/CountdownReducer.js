const SET_COUNTDOWN = 'countdown/SET_COUNTDOWN';
const SET_IS_RUN_FLAG = 'countdown/SET_IS_RUN_FLAG';
const SET_RESTART_FLAG = 'countdown/SET_RESTART_FLAG';

const initialState = {
    countdown: 0,
    isRun: false,
    restart: false
};

const countdownReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COUNTDOWN:
            return { ...state, countdown: action.countdown };
        case SET_IS_RUN_FLAG:
            return { ...state, isRun: action.isRun };
        case SET_RESTART_FLAG:
            return { ...state, restart: action.restart };
        default:
            return state;
    }
};

export const setCountdown = (countdown) => ({ type: SET_COUNTDOWN, countdown });
export const setIsRunFlag = (isRun) => ({ type: SET_IS_RUN_FLAG, isRun });
export const setRestartFlag = (restart) => ({ type: SET_RESTART_FLAG, restart });

export default countdownReducer;