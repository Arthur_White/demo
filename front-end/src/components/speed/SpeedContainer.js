import Speed from './Speed';
import { connect } from 'react-redux';
import { setSpeed } from '../../redux/SpeedReducer';

const mapStateToProps = (state) => ({
    speed: state.speed.speed
});

export default connect(mapStateToProps, {
    setSpeed,
})(Speed);