import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const Speed = (props) => {
    const onSpeedClick = (speed) => {
        props.setSpeed(speed);
    };

    const button = (speed) => (
        <Button
            className={'button' + speed}
            onClick={() => onSpeedClick(speed)}
            variant="contained"
            color={props.speed === speed ? 'primary' : 'default'}
        >
            {speed}X
        </Button>
    );

    return (
        <Grid
            style={{ paddingTop: 30 }}
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={1}
        >
            <Grid item>
                {button(1)}
            </Grid>
            <Grid item>
                {button(1.5)}
            </Grid>
            <Grid item>
                {button(2)}
            </Grid>
        </Grid>
    );
};

export default Speed;