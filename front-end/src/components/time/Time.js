import React, { useEffect, useState } from 'react';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import PauseIcon from '@material-ui/icons/Pause';
import Typography from '@material-ui/core/Typography';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

import AlertText from './AlertText';
import { useInterval, useAudio } from './Utils.js';

import url from '../../sound.wav';

const Countdown = (props) => {
    const [playing, toggle] = useAudio(url);
    const [timeLeft, setTimeLeft] = useState(props.countdown);
    const interval = 250;

    useEffect(() => {
        setTimeLeft(props.countdown);
    }, [props.countdown, props.restart]);

    useInterval(() => {
        if (timeLeft !== 0 && props.isRun) {
            setTimeLeft(timeLeft - interval);
            if (timeLeft - interval === 0) {
                setTimeout(() => toggle(), interval / props.speed);
            }
        }
    }, interval / props.speed);

    const showTime = () => {
        let minutes = Math.floor(timeLeft / 60 / 1000);
        let seconds = new Date(timeLeft).getSeconds();
        minutes = (minutes < 10 ? '0' : '') + '' + minutes;
        seconds = (seconds < 10 ? '0' : '') + '' + seconds;
        return minutes + ':' + seconds;
    };

    return (
        <Grid
            style={{ paddingTop: 20 }}
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            <Grid item style={{ height: 30 }}>
                <AlertText
                    countdown={props.countdown}
                    timeLeft={timeLeft}
                />
            </Grid>
            <Grid item>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={6}
                >
                    <Grid item sm={2}>
                        <div style={{ width: 50, height: 50 }}></div>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <Typography className='time' variant="h1" style={{ fontSize: 150, textAlign: 'center' }}>
                            {showTime()}
                        </Typography>
                    </Grid>
                    <Grid item sm={2}>
                        <Fab className='pause' onClick={() => props.setIsRunFlag(!props.isRun)}>
                            {props.isRun ? <PauseIcon /> : <PlayArrowIcon />}
                        </Fab>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Countdown;