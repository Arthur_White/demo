import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import AlertText from './AlertText';

Enzyme.configure({ adapter: new Adapter() });

describe('AlertText component', () => {
    const props = {
        countdown: 600000,
        timeLeft: 0
    };

    it('text on times up', () => {
        const alertText = shallow(<AlertText {...props} />);
        expect(alertText.find('i').text()).toEqual('Time’s up!');
    });

    it('empty text', () => {
        const newProps = {
            ...props,
            timeLeft: 600000
        };
        const alertText = shallow(<AlertText {...newProps} />);
        expect(alertText.find('.text').text()).toEqual('');
    });

    it('more then halfway text', () => {
        const newProps = {
            ...props,
            timeLeft: props.countdown / 2 - 1
        };
        const alertText = shallow(<AlertText {...newProps} />);
        expect(alertText.find('.text').text()).toEqual('More then halfway there!');
    });

    it('red text when < 20 sec', () => {
        const newProps = {
            ...props,
            timeLeft: 20 * 1000 - 1
        };
        const alertText = shallow(<AlertText {...newProps} />);
        expect(alertText.find('.red').text()).toEqual('More then halfway there!');
    });

    it('blink text when < 10 sec', () => {
        const newProps = {
            ...props,
            timeLeft: 10 * 1000 - 1
        };
        const alertText = shallow(<AlertText {...newProps} />);
        expect(alertText.find('.blink').text()).toEqual('More then halfway there!');
    });
});