import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import Time from './Time';

Enzyme.configure({ adapter: new Adapter() });

describe('Time component', () => {
    const props = {
        countdown: 600000,
        restart: false,
        isRun: true,
        speed: 1,
        setIsRunFlag: jest.fn()
    };

    it('countdown equal 10 min', () => {
        const time = shallow(<Time {...props} />);
        expect(time.find('.time').text()).toEqual('10:00');
    });

    it('click on pause button', () => {
        const time = shallow(<Time {...props} />);
        time.find('.pause').simulate('click');
        expect(props.setIsRunFlag).toHaveBeenCalledTimes(1);
    });
});