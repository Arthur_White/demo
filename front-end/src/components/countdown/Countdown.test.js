import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import Countdown from './Countdown';

Enzyme.configure({ adapter: new Adapter() });

describe('Countdown component', () => {
    const props = {
        restart: true,
        setCountdown: jest.fn(),
        setIsRunFlag: jest.fn(),
        setRestartFlag: jest.fn()
    };

    it('start countdown with incorrect data', () => {
        const countdown = shallow(<Countdown {...props} />);
        countdown.find('.input').simulate('change', { target: { value: 'qwe' } });
        countdown.find('.button').simulate('click');
        expect(props.setCountdown).not.toHaveBeenCalled();
        expect(props.setIsRunFlag).not.toHaveBeenCalled();
        expect(props.setRestartFlag).not.toHaveBeenCalled();
    });

    it('start countdown with correct data', () => {
        const countdown = shallow(<Countdown {...props} />);
        countdown.find('.input').simulate('change', { target: { value: '1' } });
        countdown.find('.button').simulate('click');
        expect(props.setCountdown).toHaveBeenCalledTimes(1);
        expect(props.setIsRunFlag).toHaveBeenCalledTimes(1);
        expect(props.setRestartFlag).toHaveBeenCalledTimes(1);
    });
});