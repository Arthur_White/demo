import Countdown from './Countdown';
import { connect } from 'react-redux';
import { setCountdown, setIsRunFlag, setRestartFlag } from '../../redux/CountdownReducer';

const mapStateToProps = (state) => ({
    restart: state.countdown.restart
});

export default connect(mapStateToProps, {
    setCountdown,
    setIsRunFlag,
    setRestartFlag
})(Countdown);